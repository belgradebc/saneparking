package rs.belgradebc.saneparking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DelayedSendActivity extends AppCompatActivity {

    BroadcastReceiver broadcastReceiverSent;
    BroadcastReceiver broadcastReceiverDelivered;
    private TextView textViewStatus;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setContentView(R.layout.activity_delayed_send);

        textViewStatus = findViewById(R.id.textViewStatus);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        if (getIntent().getBooleanExtra("SendSMS", false)) {
            if (sharedPreferences.getBoolean(MainActivity.SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false)) {
                sharedPreferences.edit().putBoolean(MainActivity.SHARED_PREFERENCES_EVER_SENT, true).apply();
                sharedPreferences.edit().putBoolean(MainActivity.SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false).apply();

                String tmpRecepient = getIntent().getStringExtra("valueRecipient");
                String tmpTablice = getIntent().getStringExtra("valueTablice");
                sendSMS(tmpRecepient, tmpTablice);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                String strDate = dateFormat.format(Calendar.getInstance().getTime());   //to convert Date to String, use format method of SimpleDateFormat class.
                textViewStatus.setText(getString(R.string.delayed_sms_was_sent_now_to) + " " + tmpRecepient + "(" + tmpTablice + ")\n" + strDate);
                MainActivity.pendingIntentToCancel = null;
                getIntent().removeExtra("SendSMS");
                //getIntent().removeExtra("valueRecipient");
                //getIntent().removeExtra("valueTablice");
            }
        } else {
            //textViewStatus.setText("SMS was already sent \n I told you to kill me WHY DIDN'T YOU LISTEN!!! \n ...thankfully nothing happened");
        }
        super.onResume();
    }

    //---sends an SMS message to another device---
    private void sendSMS(String phoneNumber, String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);


        //---when the SMS has been sent---
        broadcastReceiverSent = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
                //unregisterReceiver(broadcastReceiverSent); //done on onPause
            }
        };
        registerReceiver(broadcastReceiverSent, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        broadcastReceiverDelivered = new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
                        break;
                }
                //unregisterReceiver(broadcastReceiverDelivered); //done on onPause
            }
        };
        registerReceiver(broadcastReceiverDelivered, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(broadcastReceiverSent);
            unregisterReceiver(broadcastReceiverDelivered);
        } catch (Exception ignored) {

        }

        super.onPause();
    }
}

