package rs.belgradebc.saneparking;

import java.io.Serializable;

public class Model implements Serializable {

    private String valueTablice;
    private String valueRecipient;

    public String getValueTablice() {
        return valueTablice;
    }

    public void setValueTablice(String valueTablice) {
        this.valueTablice = valueTablice;
    }

    public String getValueRecipient() {
        return valueRecipient;
    }

    public void setValueRecipient(String valueRecipient) {
        this.valueRecipient = valueRecipient;
    }
}
