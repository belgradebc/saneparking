package rs.belgradebc.saneparking;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SpecialCodesActivity extends AppCompatActivity {

    EditText editTextSpecialCode;
    static final String SHARED_PREFERENCES_REMOVE_ADS = "SHARED PREFERENCES REMOVE ADS";
    static final String REMOVE_FIRST_TIME_WARNING = "REMOVE FIRST TIME WARNING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setContentView(R.layout.activity_special_codes);
        editTextSpecialCode = findViewById(R.id.editTextSpecialCode);
    }

    public void onClickGoSpecialCode(View view) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        switch (editTextSpecialCode.getText().toString().toUpperCase()) {
            case SHARED_PREFERENCES_REMOVE_ADS:
                if (sharedPreferences.getBoolean(SHARED_PREFERENCES_REMOVE_ADS, false)) {
                    sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_REMOVE_ADS, false).apply();
                } else {
                    sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_REMOVE_ADS, true).apply();
                }
                break;
            case REMOVE_FIRST_TIME_WARNING:
                sharedPreferences.edit().putBoolean(MainActivity.SHARED_PREFERENCES_EVER_SENT, true).apply();
                Toast.makeText(this, R.string.first_time_warning_removed, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }
}
