package rs.belgradebc.saneparking;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class AreYouSureDialogFragment extends AppCompatDialogFragment {
    String dialogMessage = "";

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        setStyle(AppCompatDialogFragment.STYLE_NORMAL, R.style.Dialog);
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.Dialog);
        builder.setTitle(R.string.are_you_sure)
                .setIcon(R.mipmap.are_you_sure_icon)
                .setMessage(dialogMessage)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        mListener.onAreYouSureDialogPositiveClick(AreYouSureDialogFragment.this);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        mListener.onAreYouSureDialogNegativeClick(AreYouSureDialogFragment.this);
                    }
                });
        if (mListener.isDelayed() && !mListener.isEverSent()) {
            builder.setMessage(dialogMessage + "\n" + getString(R.string.are_you_sure_message_first))
                    .setPositiveButton(R.string.ipak_posalji, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // FIRE ZE MISSILES!
                            mListener.onAreYouSureDialogPositiveClick(AreYouSureDialogFragment.this);
                        }
                    })
                    .setNegativeButton(R.string.ok_odustani, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            mListener.onAreYouSureDialogNegativeClick(AreYouSureDialogFragment.this);
                        }
                    });
        } else if (mListener.isDelayed() && mListener.isDelayedMessageQueued()) {
            builder.setMessage(dialogMessage + "\n" + getString(R.string.are_you_sure_message_dequeue));
        }

        // Create the AlertDialog object and return it
        return builder.create();
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface AreYouSureDialogListener {
        void onAreYouSureDialogPositiveClick(AppCompatDialogFragment dialog);

        void onAreYouSureDialogNegativeClick(AppCompatDialogFragment dialog);

        boolean isDelayed();

        boolean isDelayedMessageQueued();

        boolean isEverSent();
    }

    // Use this instance of the interface to deliver action events
    AreYouSureDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the AreYouSureDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the AreYouSureDialogListener so we can send events to the host
            mListener = (AreYouSureDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement AreYouSureDialogListener");
        }
    }

    public void setDialogMessage(String message) {
        dialogMessage = message;
    }

}
