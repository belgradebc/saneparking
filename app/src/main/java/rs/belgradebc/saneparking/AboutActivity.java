package rs.belgradebc.saneparking;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class AboutActivity extends AppCompatActivity {

    TextView textView;
    static int cnt;
    static final int goal = 10;
    static final int ultraGoal = 100;
    static final int startToastFrom = 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setContentView(R.layout.activity_about);
        textView = findViewById(R.id.aboutTextView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        textView.setText(R.string.posveta);
        cnt = 0;
    }

    public void onClickPosveta(View view) {
        ++cnt;
        if (cnt >= startToastFrom && cnt < goal) {
            Toast.makeText(this, "" + (goal - cnt), Toast.LENGTH_SHORT).show();
        }
        if (cnt == goal) {
            textView.setText(R.string.posveta_easter_egg);
        }
        if (cnt == ultraGoal){
            textView.setText(R.string.posveta_easter_egg_ultra_goal);
        }
    }
}
