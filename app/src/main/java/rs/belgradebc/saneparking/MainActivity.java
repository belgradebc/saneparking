package rs.belgradebc.saneparking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.SwitchCompat;

import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity
        extends
        AppCompatActivity
        implements
        AreYouSureDialogFragment.AreYouSureDialogListener {

    static final String SHARED_PREFERENCES_LOCALE = "SHARED_PREFERENCES_LOCALE";
    static final String SHARED_PREFERENCES_SWITCH_DELAY = "delaySwitchKey";
    static final String SHARED_PREFERENCES_SWITCH_FORCE = "lemmeSwitchKey";
    static final String SHARED_PREFERENCES_EVER_SENT = "SHARED_PREFERENCES_EVER_SENT";
    static final String SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED = "SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED";
    static final String SHARED_PREFERENCES_TABLICE_KEY = "TABLICE_KEY";
    static final String SHARED_PREFERENCES_CUSTOM_DELAY = "EDIT_TEXT_SETTINGS_CUSTOM_DELAY_SECONDS";
    static final String SHARED_PREFERENCES_SHARED_PREFERENCES_VERSION = "SHARED_PREFERENCES_SHARED_PREFERENCES_VERSION";

    static final int MY_PERMISSIONS_REQUEST_SMS = 5678;
    static final int PENDING_INTENT_REQUEST_CODE = 1234;
    static final int MIN5_IN_SEC = 5 * 60;

    static PendingIntent pendingIntentToCancel = null;
    static Model model;

    private TextView textViewStatus;
    private TextView textViewTablice;
    private Switch switchDelay;
    private Switch switchForce;
    private SharedPreferences sharedPreferences;
    private Button button9111;
    private Button button9112;
    private Button button9113;
    private Button button9119;
    private Button button9118;
    private Button button9114;

    private boolean isMessageBeingScheduled = false;
    private Calendar scheduledDateTime = null;

    private void updateSharedPreferences() {
        int sharedPreferencesVersion = sharedPreferences.getInt(SHARED_PREFERENCES_SHARED_PREFERENCES_VERSION, 0);
        switch (sharedPreferencesVersion) {
            case 0:
                updateSharedPreferences0to1();
            case 1:
                updateSharedPreferences1to2();
                break;
        }
    }

    @SuppressLint("ApplySharedPref")
    private void updateSharedPreferences0to1() {
        boolean booleanSwitchDelay = sharedPreferences.getString(SHARED_PREFERENCES_SWITCH_DELAY, "").equals("true");
        sharedPreferences.edit().remove(SHARED_PREFERENCES_SWITCH_DELAY).commit();
        sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_SWITCH_DELAY, booleanSwitchDelay).commit();

        boolean booleanSwitchForce = sharedPreferences.getString(SHARED_PREFERENCES_SWITCH_FORCE, "").equals("true");
        sharedPreferences.edit().remove(SHARED_PREFERENCES_SWITCH_FORCE).commit();
        sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_SWITCH_FORCE, booleanSwitchForce).commit();

        sharedPreferences.edit().putInt(SHARED_PREFERENCES_SHARED_PREFERENCES_VERSION, 1).commit();
    }

    @SuppressLint("ApplySharedPref")
    private void updateSharedPreferences1to2() {
        long secToDelayLong = sharedPreferences.getLong(SHARED_PREFERENCES_CUSTOM_DELAY, 0);
        sharedPreferences.edit().remove(SHARED_PREFERENCES_CUSTOM_DELAY).commit();

        int secToDelayInt;
        if (secToDelayLong > Integer.MAX_VALUE) {
            secToDelayInt = MIN5_IN_SEC;
        } else {
            secToDelayInt = (int) secToDelayLong;
        }

        sharedPreferences.edit().putInt(SHARED_PREFERENCES_CUSTOM_DELAY, secToDelayInt).commit();
        sharedPreferences.edit().putInt(SHARED_PREFERENCES_SHARED_PREFERENCES_VERSION, 2).commit();
    }

    private void saveAppState() {
        sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_SWITCH_FORCE, switchForce.isChecked()).apply();
        sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_SWITCH_DELAY, switchDelay.isChecked()).apply();
    }

    private void restoreAppState() {
        String tablice_string = sharedPreferences.getString(SHARED_PREFERENCES_TABLICE_KEY, "");
        textViewTablice.setText(tablice_string);

        switchDelay.setChecked(sharedPreferences.getBoolean(SHARED_PREFERENCES_SWITCH_DELAY, false));
        switchForce.setChecked(sharedPreferences.getBoolean(SHARED_PREFERENCES_SWITCH_FORCE, false));

        textViewStatus.setText("");
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveAppState();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("model", model);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            model = (Model) savedInstanceState.getSerializable("model");
        } else {
            model = new Model();
        }
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        updateSharedPreferences();
        preSetLocale(sharedPreferences.getString(SHARED_PREFERENCES_LOCALE, "en"));
        setContentView(R.layout.activity_main);

        textViewStatus = findViewById(R.id.textViewStatus);
        textViewTablice = findViewById(R.id.textViewTablice);
        switchDelay = findViewById(R.id.switchDelay);
        switchForce = findViewById(R.id.switchForce);

        button9111 = findViewById(R.id.zona1);
        button9112 = findViewById(R.id.zona2);
        button9113 = findViewById(R.id.zona3);
        button9119 = findViewById(R.id.zonaPlava);
        button9118 = findViewById(R.id.zonaPlavaDnevna);
        button9114 = findViewById(R.id.zonaA);

        button9111.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });

        button9112.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });

        button9113.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });

        button9119.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });

        button9118.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });

        button9114.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                MainActivity.this.onSendClickedLogic(view, true);
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        if (missingSomePermissions()) {
            requestAllPermissions();
        }
        restoreAppState();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        preSetLocale(sharedPreferences.getString(SHARED_PREFERENCES_LOCALE, "en"));
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void onClickStatus(View view) {
        //textViewStatus.setText(R.string.status_cleared);
    }

    public boolean isParkingBeingPaidForAtDestinationTime(long delayInMillis) {
        if (switchForce.isChecked()) {
            return true;
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis() + delayInMillis);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        boolean validTime = true;
        switch (day) {
            case Calendar.MONDAY:
            case Calendar.TUESDAY:
            case Calendar.WEDNESDAY:
            case Calendar.THURSDAY:
            case Calendar.FRIDAY:
                if (hour < 7 || hour > 20 || (hour == 20 && minute >= 45)) {
                    validTime = false;
                }
                break;
            case Calendar.SATURDAY:
                if (hour < 7 || hour > 13 || (hour == 13 && minute >= 45)) {
                    validTime = false;
                }
                break;
            case Calendar.SUNDAY:
                validTime = false;
                break;
        }
        return validTime;
    }

    public boolean missingSomePermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (checkSelfPermission("android.permission.SEND_SMS") != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission("android.permission.READ_SMS") != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission("android.permission.RECEIVE_SMS") != PackageManager.PERMISSION_GRANTED) ||
                    (checkSelfPermission("android.permission.READ_PHONE_STATE") != PackageManager.PERMISSION_GRANTED);
        }
        return false;
    }

    public void requestAllPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_SMS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean shouldShowToast = false;
        for (int i : grantResults) {
            if (i == PackageManager.PERMISSION_DENIED) {
                shouldShowToast = true;
                break;
            }
        }
        if (shouldShowToast) {
            //Nista se ne prikazuje jer je nemoguce koristiti app bez permission-a (jer se requestAllPermissions nalazi u onResume() a ne u onCreate())
            //Toast.makeText(getBaseContext(), "Niste dozvolili sve permission-e za slanje SMS-a, kliknite na bilo koje dugme za slanje da dozvolite ili ponovo pokrenite aplikaciju", Toast.LENGTH_LONG).show();
        }
        //Toast.makeText(getBaseContext(), "SMS se nije poslao usled trazenja permissions-a, kliknite dugme ponovo", Toast.LENGTH_SHORT).show();
    }

    public void onSendClickedLogic(View view, boolean isLongClick) {
        if (missingSomePermissions()) {
            requestAllPermissions();
            return;
        }

        model.setValueTablice(textViewTablice.getText().toString());
        if (model.getValueTablice().isEmpty()) {
            Toast.makeText(getBaseContext(), getString(R.string.niste_uneli_tablice), Toast.LENGTH_SHORT).show();
            return;
        }

        switch (view.getId()) {
            case R.id.zona1:
                model.setValueRecipient("9111");
                break;
            case R.id.zona2:
                model.setValueRecipient("9112");
                break;
            case R.id.zona3:
                model.setValueRecipient("9113");
                break;
            case R.id.zonaPlava:
                model.setValueRecipient("9119");
                break;
            case R.id.zonaPlavaDnevna:
                model.setValueRecipient("9118");
                break;
            case R.id.zonaA:
                model.setValueRecipient("9114");
                break;
            default:
                throw new RuntimeException();
        }

        if (isLongClick) {
            showScheduleMessageDialog();
        } else {
            showAreYouSureDialog();
        }
    }

    public void onSendClicked(View view) {
        onSendClickedLogic(view, false);
    }

    //---sends an SMS message to another device---
    private void sendSMS(String phoneNumber, String message) {

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
                        sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_EVER_SENT, true).apply();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }

    private void sendDelayedSmsInternal(String phoneNumber, String message, long triggerAtMillis) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            AlarmManager alarmManager = getSystemService(AlarmManager.class);

            Toast.makeText(this, getString(R.string.delaying_until) + " " + getTimestampAndDelta(triggerAtMillis), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, DelayedSendActivity.class);
            intent.putExtra("SendSMS", true);
            intent.putExtra("valueRecipient", phoneNumber);
            intent.putExtra("valueTablice", message);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, PENDING_INTENT_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, triggerAtMillis, pendingIntent);
            pendingIntentToCancel = pendingIntent;
            sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, true).apply();
        }
    }

    public void showAreYouSureDialog() {
        // Create an instance of the dialog fragment and show it
        AreYouSureDialogFragment dialog = new AreYouSureDialogFragment();

        // Build dialog message
        String dialogMessage = model.getValueTablice() + " ---> " + model.getValueRecipient();
        long delayInMillis = 0;
        if (isMessageBeingScheduled || switchDelay.isChecked()) {

            if (isMessageBeingScheduled) {
                delayInMillis = scheduledDateTime.getTimeInMillis() - System.currentTimeMillis();
            } else if (switchDelay.isChecked()) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                delayInMillis = sharedPreferences.getInt(SHARED_PREFERENCES_CUSTOM_DELAY, MIN5_IN_SEC) * 1000;
            } else {
                throw new RuntimeException("Not Implemented");
            }

            dialogMessage += "\n" + getTimestampAndDelta(delayInMillis + System.currentTimeMillis());
        } else {
            dialogMessage += "\n" + getString(R.string.immediately);
        }

        if (!isParkingBeingPaidForAtDestinationTime(delayInMillis)) {
            preSetLocale(sharedPreferences.getString(SHARED_PREFERENCES_LOCALE, "en"));
            Toast.makeText(getBaseContext(), getString(R.string.trenutno_se_ne_placa_parking), Toast.LENGTH_SHORT).show();
            return;
        }

        dialog.setDialogMessage(dialogMessage);
        dialog.show(getSupportFragmentManager(), "AreYouSureDialogFragment");
    }

    public void showScheduleMessageDialog() {
        // Create an instance of the dialog fragment and show it
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                MainActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, final int years, final int months, final int days) {
                        final TimePickerDialog timePickerDialog = new TimePickerDialog(
                                MainActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                        Calendar destinationDateTime = Calendar.getInstance();
                                        Calendar current = Calendar.getInstance();
                                        destinationDateTime.set(Calendar.YEAR, years);
                                        destinationDateTime.set(Calendar.MONTH, months);
                                        destinationDateTime.set(Calendar.DAY_OF_MONTH, days);
                                        destinationDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                        destinationDateTime.set(Calendar.MINUTE, minute);
                                        // buffer time is used to prevent the user to schedule an SMS to close to now.
                                        int bufferTimeInMillis = 60 * 1000;
                                        if (destinationDateTime.getTimeInMillis() >= current.getTimeInMillis() + bufferTimeInMillis) {
                                            //it's after current
                                            scheduledDateTime = Calendar.getInstance();
                                            scheduledDateTime.set(years, months, days, hourOfDay, minute);


                                            isMessageBeingScheduled = true;
                                            showAreYouSureDialog();
                                        } else {
                                            //it's before current
                                            Toast.makeText(getApplicationContext(), R.string.select_time_is_in_the_past, Toast.LENGTH_LONG).show();
                                        }
                                    }
                                },
                                calendar.get(Calendar.HOUR_OF_DAY),
                                calendar.get(Calendar.MINUTE),
                                DateFormat.is24HourFormat(MainActivity.this));

                        timePickerDialog.show();
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.AreYouSureDialogListener interface
    @SuppressLint("SetTextI18n")
    @Override
    public void onAreYouSureDialogPositiveClick(AppCompatDialogFragment dialog) {
        // User touched the dialog's positive button
        if (isMessageBeingScheduled || switchDelay.isChecked()) {
            //delayed FIRE ZE MISSILES
            long triggerAtMillis;
            long delayInMillis;

            if (isMessageBeingScheduled) {
                isMessageBeingScheduled = false;
                triggerAtMillis = scheduledDateTime.getTimeInMillis();
                // delayInMillis = scheduledDateTime.getTimeInMillis() - System.currentTimeMillis();
            } else if (switchDelay.isChecked()) {
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                delayInMillis = sharedPreferences.getInt(SHARED_PREFERENCES_CUSTOM_DELAY, MIN5_IN_SEC) * 1000;
                triggerAtMillis = System.currentTimeMillis() + delayInMillis;
            } else {
                throw new RuntimeException("NotYetImplemented");
            }

            sendDelayedSmsInternal(model.getValueRecipient(), model.getValueTablice(), triggerAtMillis);
            textViewStatus.setText(getString(R.string.delaying) + " " + model.getValueRecipient() + " ---> " + model.getValueTablice() + "\n" + getTimestampAndDelta(triggerAtMillis));
        } else {
            //non-delayed FIRE ZE MISSILES!
            sendSMS(model.getValueRecipient(), model.getValueTablice());
            textViewStatus.setText(getString(R.string.sent) + " " + model.getValueRecipient() + " ---> " + model.getValueTablice() + "\n" + getString(R.string.immediately));
        }
    }

    @Override
    public void onAreYouSureDialogNegativeClick(AppCompatDialogFragment dialog) {
        // User touched the dialog's negative button
        Toast.makeText(getBaseContext(), R.string.odustali_ste, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isDelayed() {
        return switchDelay.isChecked();
    }

    @Override
    public boolean isDelayedMessageQueued() {
        return (sharedPreferences.getBoolean(SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false));
    }

    @Override
    public boolean isEverSent() {
        return sharedPreferences.getBoolean(SHARED_PREFERENCES_EVER_SENT, false);
    }

    public void onClickOpenSettings(View view) {
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        startActivity(settingsIntent);
    }

    public void onClickMainMenuSettings(MenuItem item) {
        onClickOpenSettings(null);
    }

    public void onClickCancelDelayedSms(View view) {
        if (!sharedPreferences.getBoolean(SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false)) {
            textViewStatus.setText("");
            Toast.makeText(this, R.string.no_sms_queued, Toast.LENGTH_SHORT).show();
            return;
        }
        if (pendingIntentToCancel != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                AlarmManager alarmManager = getSystemService(AlarmManager.class);
                if (alarmManager == null) {
                    Toast.makeText(this, "Could not obtain AlarmManager", Toast.LENGTH_SHORT).show();
                    return;
                }
                alarmManager.cancel(pendingIntentToCancel);
                sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false).apply();
                pendingIntentToCancel = null;
                Toast.makeText(this, R.string.delayed_sms_canceled, Toast.LENGTH_SHORT).show();
                textViewStatus.setText(R.string.delayed_sms_canceled);
            }
        } else {
            Intent intent = new Intent(this, DelayedSendActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, PENDING_INTENT_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmManager;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                alarmManager = getSystemService(AlarmManager.class);
            } else {
                alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            }
            if (alarmManager == null) {
                Toast.makeText(this, "Could not obtain AlarmManager", Toast.LENGTH_SHORT).show();
                return;
            }
            alarmManager.cancel(pendingIntent);
            sharedPreferences.edit().putBoolean(SHARED_PREFERENCES_IS_DELAYED_MESSAGE_QUEUED, false).apply();
            /*
            //<overwrite> //turns out... ovo je nepotrebno, ostavlja se kod na neko vreme cisto da bude tu koji commit
            int secToDelay = 900;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + secToDelay * 1000, pendingIntent);
            alarmManager.cancel(pendingIntent);
            //</overwrite>
            */
            Toast.makeText(this, getString(R.string.delayed_sms_canceled) + ".", Toast.LENGTH_SHORT).show();
            textViewStatus.setText(R.string.delayed_sms_canceled);
        }
    }


    public void onClickMainMenuAbout(MenuItem item) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    public void onClickSpecialCodes(MenuItem item) {
        Intent intent = new Intent(this, SpecialCodesActivity.class);
        startActivity(intent);
    }

    public void onClickZoneLookup(MenuItem item) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.zone_lookup_website)));
        startActivity(browserIntent);
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, MainActivity.class);
        startActivity(refresh);
        finish();
    }

    public void preSetLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    public void onClickMainMenuChangeLanguage(MenuItem item) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Locale current = getResources().getConfiguration().locale;
        if (current.getLanguage().equals(new Locale("sr").getLanguage())) {
            setLocale("en");
            sharedPreferences.edit().putString(SHARED_PREFERENCES_LOCALE, "en").apply();
        } else {
            setLocale("sr");
            sharedPreferences.edit().putString(SHARED_PREFERENCES_LOCALE, "sr").apply();
        }
    }

    public void onClickPanic(MenuItem item) {
        Intent intent = new Intent(this, PanicActivity.class);
        startActivity(intent);
    }

    /**
     * Build a user friendly string from an amount of time units.
     *
     * @param field  the calendar field. e.g. <code>Calendar.SECONDS</code>
     * @param amount the amount of date or time to be added to the field.
     * @return string in format '2d 16h 15s'
     */
    private String buildTimeDelta(int field, long amount) {
        Calendar current = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();
        if (amount > Integer.MAX_VALUE) {
            if (field == Calendar.MILLISECOND) {
                date2.setTimeInMillis(System.currentTimeMillis() + amount);
            } else {
                throw new RuntimeException("Long to Int overflow. Tell the author to fix the implementation.");
            }
        } else {
            date2.add(field, (int) amount);
        }

        long deltaInMillis = date2.getTimeInMillis() - current.getTimeInMillis();
        long deltaInDays = deltaInMillis / 1000 / 60 / 60 / 24;
        long deltaInHours = deltaInMillis / 1000 / 60 / 60 % 24;
        long deltaInMinutes = deltaInMillis / 1000 / 60 % 60;
        long deltaInSeconds = deltaInMillis / 1000 % 60;

        StringBuilder stringBuilder = new StringBuilder();
        if (deltaInDays > 0) {
            stringBuilder.append(deltaInDays);
            stringBuilder.append("d ");
        }
        if (deltaInHours > 0) {
            stringBuilder.append(deltaInHours);
            stringBuilder.append("h ");
        }
        if (deltaInMinutes > 0) {
            stringBuilder.append(deltaInMinutes);
            stringBuilder.append("min ");
        }
        if (deltaInSeconds > 0) {
            stringBuilder.append(deltaInSeconds);
            stringBuilder.append("s ");
        }

        return stringBuilder.toString();
    }

    private String getTimestampAndDelta(long destinationTimestampInMillis) {
        Calendar calendar = Calendar.getInstance();
        String delta = buildTimeDelta(Calendar.MILLISECOND, destinationTimestampInMillis - calendar.getTimeInMillis());

        calendar.setTimeInMillis(destinationTimestampInMillis);
        String pattern;
        if (DateFormat.is24HourFormat(this)) {
            pattern = "dd/MM 'at' HH:mm";
        } else {
            pattern = "dd/MM 'at' hh:mm a";
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        String timestamp = format.format(calendar.getTime());

        return timestamp +
                "\n=" + getString(R.string.word_in_za) + " " + delta;
    }
}
