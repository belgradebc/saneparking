package rs.belgradebc.saneparking;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    private EditText editTextCustomDelay;
    private EditText editTextTablice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setContentView(R.layout.activity_settings);


        //mozda treba globalni shared preferences
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        editTextCustomDelay = findViewById(R.id.editTextSettingsCustomDelaySeconds);
        int fetchedCustomDelayInSeconds = sharedPreferences.getInt(MainActivity.SHARED_PREFERENCES_CUSTOM_DELAY, 0);
        if (fetchedCustomDelayInSeconds != 0) {
            editTextCustomDelay.setText("" + fetchedCustomDelayInSeconds);
        }
        editTextCustomDelay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int customDelay = Integer.parseInt(s.toString());
                    if (customDelay < 0) {
                        throw new Exception("Ne moze negativan broj");
                    }
                    sharedPreferences.edit().putInt(MainActivity.SHARED_PREFERENCES_CUSTOM_DELAY, customDelay).apply();
                } catch (NumberFormatException ex) {
                    sharedPreferences.edit().remove(MainActivity.SHARED_PREFERENCES_CUSTOM_DELAY).apply();
                } catch (Exception ex) {
                    Toast.makeText(SettingsActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                    sharedPreferences.edit().remove(MainActivity.SHARED_PREFERENCES_CUSTOM_DELAY).apply(); //resetuje na 900
                }
            }
        });

        editTextTablice = findViewById(R.id.editTextSettingsTablice);
        String fetchedTablice = sharedPreferences.getString(MainActivity.SHARED_PREFERENCES_TABLICE_KEY, "");
        if (!fetchedTablice.isEmpty()) {
            editTextTablice.setText(fetchedTablice);
        }
        editTextTablice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    sharedPreferences.edit().putString(MainActivity.SHARED_PREFERENCES_TABLICE_KEY, s.toString().toUpperCase()).apply();
                } catch (Exception ex) {
                    Toast.makeText(SettingsActivity.this, ex.toString(), Toast.LENGTH_SHORT).show();
                    sharedPreferences.edit().remove(MainActivity.SHARED_PREFERENCES_TABLICE_KEY).apply();
                }
            }
        });
    }
}
