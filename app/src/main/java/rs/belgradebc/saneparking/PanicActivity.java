package rs.belgradebc.saneparking;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class PanicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        }
        setContentView(R.layout.activity_panic);
    }
}
